<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAFh1h3cY:APA91bF_4vjHkU5hB1qZGOgyDLm9Zqd8RFsbIlqjEHduCk896VbdfyteRPAi4Hcg71ZbG2jlXljMd9f61dKimOzg1bXKn1CZZTH0jgAuK6zjoa3GXv2B_0CgikhrU4uC9Sh3HKtXiY9w'),
        'sender_id' => env('FCM_SENDER_ID', '94982233542'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
