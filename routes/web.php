<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return response()->json($router->app->version());
});

$router->group(['prefix' => 'auth', 'namespace' => 'Auth'], function() use ($router) {
    $router->post('signin', 'AuthController@signin');
    $router->post('forgot-password', 'AuthController@forgotPassword');
    $router->get('reset-password/{token}', 'AuthController@resetPassword');
    $router->get('verify-reset-password', 'AuthController@verifyResetPassword');
    $router->get('verify-account/{token}', 'AuthController@verifyAccount');
    $router->post('verify-otp', 'AuthController@verifyOTP');
    $router->post('resend-otp', 'AuthController@resendOTP');
});

$router->post('member', 'Member\MemberController@create');
$router->get('branch/member-branch', 'Branch\BranchController@memberBranch');

$router->group(['middleware' => 'jwt.auth'], function() use ($router){
    $router->group(['prefix' => 'auth', 'namespace' => 'Auth'], function() use ($router){
        $router->post('change-password', 'AuthController@changePassword');
        $router->get('resend-email-verification', 'AuthController@resendEmailVerification');
        $router->get('get-auth', 'AuthController@getAuth');
        $router->post('refresh-fcm-token', 'AuthController@refreshFCMToken');
    });

    $router->group(['prefix' => 'member', 'namespace' => 'Member'], function() use ($router){
        $router->get('/', 'MemberController@index');
    });

    $router->group(['prefix' => 'branch', 'namespace' => 'Branch'], function() use ($router){
        $router->get('/', 'BranchController@index');        
    });

    $router->group(['prefix' => 'contract-member', 'namespace' => 'Contract'], function() use ($router){
        $router->get('/', 'ContractMemberController@index');
    });
});
