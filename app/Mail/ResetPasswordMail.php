<?php

namespace App\Mail;

use App\Models\Member\MemberModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
  
    //JADI SECARA DEFAULT KITA MEMINTA DATA USER
    public function __construct(MemberModel $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('auth.resetPassword')->with(['user' => $this->user])->subject('Reset Password')->from('info@duoshinefitness.net','Duo Shine Fitness');
    }
}