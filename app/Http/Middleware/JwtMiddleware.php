<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\Models\Member\MemberModel;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('authorization');
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
               'success' => false,  
               'message' => 'Token not provided.'
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'success' => false,
                'message' => 'Provided token is expired.'
            ], 401);
        } catch(Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'An error while decoding token.'
            ], 401);
        }
        $user = MemberModel::find($credentials->sub);
        // Now let's put the user in the request class so that you can grab it from there
        $request->auth = $user;
        return $next($request);
    }
}