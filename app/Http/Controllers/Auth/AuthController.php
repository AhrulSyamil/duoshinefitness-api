<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\AuthModel;
use App\Models\Member\MemberModel;
use App\Models\Branch\BranchModel;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordMail;
use App\Mail\PasswordChanged;
use App\Mail\VerifyAccount;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }
    /**
     * Create a new token.
     * 
     * @param  \App\Models\Auth\MemberModel   $MemberModel
     * @return string
     */
    protected function jwt(MemberModel $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->member_id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60*24*30 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\Models\Auth\MemberModel   $MemberModel 
     * @return mixed
     */
    public function signin(MemberModel $user) {
        $validator = Validator::make($this->request->all(), [
            'phone'     => 'required',
            'password'  => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'error' => $validator->errors()
            ]);
        }

        $loginType = filter_var($this->request->input('phone'), FILTER_VALIDATE_EMAIL) ? 'member_email' : 'member_phone';

        $user = MemberModel::where($loginType, $this->request->input('phone'))->first();
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User does not exist.'
            ], 400);
        }

        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'success' => true,
                'token' => $this->jwt($user),
                'message' => 'Sign in successfully',
                'result' => [
                    'member_id' => $user->member_id,
                    'member_id_card' => $user->member_idcard,
                    'member_name' => $user->member_name,
                    'member_photo' => env('MEMBER_PATH_MEDIA') . '/' . $user->member_photo,
                    'ktp_photo' => env('MEMBER_KTP_PATH_MEDIA') . '/' . $user->ktp_photo,
                    'member_dob'=> $user->member_dob,
                    'member_gender' => $user->member_gender,
                    'member_phone' => $user->member_phone,
                    'member_add' => $user->member_add,
                    'member_email' => $user->member_email,
                    'branch_id' => $user->branch_id,
                    'branch_name' => BranchModel::where('branch_id', $user->branch_id)->first()->branch_name,
                    'verified_otp' => $user->verified_otp,
                    'auth_code' => $user->auth_code,
                    'verified_email' => $user->verified_email
                ]
            ], 200);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'Invalid phone/email or password'
        ], 400);
    }

    public function changePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/',
            'old_password' => 'required'
        ]);

        $validator->after(function($validator) use($request) {
            $old_password = MemberModel::where('member_id', $request->auth->member_id)->first()->password;

            if(!Hash::check($request->old_password, $old_password)) {
                $validator->errors()->add('old_password', 'The old password is incorrect');
            }
        });

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'error' => $validator->errors()
            ], 400);
        }

        try {
            DB::beginTransaction();

            $update = MemberModel::where('member_id', $request->auth->member_id)->first();
            $update->password = Hash::make($request->password);
            
            if($update->save()) {
                DB::commit();

                Mail::to($update->member_email)->send(new PasswordChanged($update));

                return response()->json([
                    'success' => true,
                    'message' => 'The password has been changed',
                    'result' => $update
                ], 200);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }

    public function forgotPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:tab_member,member_email',
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'error' => $validator->errors()
            ], 400);
        }

        try {
            DB::beginTransaction();

            $user = MemberModel::where('member_email', $request->email)->first();

            $user->update(['reset_token' => Str::random(40)]);
            // Mail::to($user->member_email)->send(new ResetPasswordMail($user));

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'If your email address exists in our system, you will receive a password recovery link at your email address in a few minutes.',
            ], 200);

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }

    public function resetPassword(Request $request, $token) {
        $user = MemberModel::where('reset_token', $token)->first();

        $verified = (!$user) ? false : true;

        return view('auth.verifyResetPassword', compact('token', 'verified'));
    }

    public function verifyResetPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/',
        ]);

        if($validator->fails()) {
            return redirect('auth/reset-password'. '/' . $request->token . '?success=false&message=' . $validator->errors()->first());
        }

        try {
            DB::beginTransaction();

            $user = MemberModel::where('reset_token', $request->token)->first();

            if(!$user) {
                return redirect('auth/reset-password/' . $request->token . '?success=false&message=Invalid token');
            }

            $user->update([
                'password' => Hash::make($request->password),
                'reset_token' => Str::random(40)
            ]);

            DB::commit();

            Mail::to($user->member_email)->send(new PasswordChanged($user));

            return redirect('auth/reset-password/' . $request->token . '?success=true&message=Your password has been changed successfully');

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }

    public function verifyAccount(Request $request, $token) {
        try {
            DB::beginTransaction();

            $member_id = Crypt::decrypt($token);

            $user = MemberModel::where('member_id', $member_id)->first();

            if(!$user) {
                return view('auth/verifiedAccount', [
                    'label' => 'danger',
                    'message' => 'Invalid token'
                ]);
            }

            $user->update([
                'verified_email' => 1
            ]);

            DB::commit();

            return view('auth/verifiedAccount', [
                'label' => 'success',
                'message' => 'Email has been verified'
            ]);
            

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }

    public function resendEmailVerification(Request $request) {
        try {
            $data = MemberModel::where('member_id', $request->auth->member_id)->first();
            $data->token = Crypt::encrypt($data->member_id);

            // Mail::to($data->member_email)->send(new verifyAccount($data));
            
            return response()->json([
                'success' => true,
                'message' => 'Email verification has been send to ' . $data->member_email ,
            ], 200);

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }

    public function verifyOTP(Request $request) {
        $otp = MemberModel::where([
            'code_otp' => $request->code_otp,
            'member_phone' => $request->phone
        ])->first();

        if(isset($otp->code_otp)) {
            if($otp->due_time_otp < date('Y-m-d H:i:s')) {
                return response()->json([
                    'success' => false,
                    'message' => 'Your OTP session has ended, please resend your OTP again'
                ]); 
            }

            $otp->verified_otp = 1;
            if($otp->save()) {
                return response()->json([
                    'success' => true,
                    'message' => 'Your account has been verified'
                ]);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Invalid code OTP'
            ]);
        }
    }

    public function resendOTP(Request $request) {
        $phone = MemberModel::where('member_phone', $request->phone)->first();

        if($phone) {

            // if(date('Y-m-d H:i:s') < $phone->due_time_otp) {
            //     return response()->json([
            //         'success' => false,
            //         'message' => 'You can resend the OTP code after five minutes (' . date('H:i', strtotime($phone->due_time_otp)) . ')'
            //     ]);
            // }

            $digit_otp = 4;
            $code_otp = rand(pow(10, $digit_otp-1), pow(10, $digit_otp)-1);
            $phone->code_otp = $code_otp;
            $phone->due_time_otp = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +5 minutes"));

            if($phone->save()) {
                // $message = 'Kode verifikasi akun DSF Anda adalah : ' . $code_otp;
                // Http::get(env('OTP_URL'), [
                //     'username' => env('OTP_USERNAME'),
                //     'password' => env('OTP_PASSWORD'),
                //     'sender' => 'Duo Shine Fitness',
                //     'msisdn' => $request->phone,
                //     'message' => $message,
                // ]);

                return response()->json([
                    'success' => true,
                    'message' => 'OTP code has been sent, please check your SMS'
                ]);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Phone number not register'
            ]);
        }
    }

    public function getAuth(Request $request) {
        $user = $request->auth;
        return response()->json([
            'success' => true,
            'message' => 'Authentication has been retrieved',
            'result' => [
                'member_id' => $user->member_id,
                'member_id_card' => $user->member_idcard,
                'member_name' => $user->member_name,
                'member_photo' => env('MEMBER_PATH_MEDIA') . '/' . $user->member_photo,
                'ktp_photo' => env('MEMBER_KTP_PATH_MEDIA') . '/' . $user->ktp_photo,
                'member_dob'=> $user->member_dob,
                'member_gender' => $user->member_gender,
                'member_phone' => $user->member_phone,
                'member_add' => $user->member_add,
                'member_email' => $user->member_email,
                'branch_id' => $user->branch_id,
                'branch_name' => BranchModel::where('branch_id', $user->branch_id)->first()->branch_name,
                'verified_otp' => $user->verified_otp,
                'auth_code' => $user->auth_code,
                'verified_email' => $user->verified_email
            ]
        ], 200);
    }

    public function refreshFCMToken(Request $request) {
        try {
            $member = MemberModel::where('member_id', $request->auth->member_id)->first();
            $member->fcm_token = $request->token;
            $member->save();
            
            return response()->json([
                'success' => true,
                'message' => 'FCM token has been refresh'
            ], 200);

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }
}