<?php

namespace App\Http\Controllers\Contract;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contract\ContractMemberModel;

class ContractMemberController extends Controller
{
    public function index(Request $request) {
        // $per_page = ($request->per_page) ? $request->per_page : 10;
        // $contract = ContractMemberModel::with(array('paket'=>function($query){
        //     $query->select('paket_id','paket_name', 'paket_value', 'duration');
        // }))->where('member_id', $member_id)->paginate($per_page);
        $contract = ContractMemberModel::with(array('paket'=> function($query){
            $query->select('paket_id','paket_name', 'paket_value', 'duration');
        }))->where('member_id', $request->auth->member_id)->orderBy('ts', 'DESC')->get();

        return response()->json([
            'success' => true,
            'message' => 'Contract member data has been retrivied',
            'result' => $contract
        ], 200);
    }
}