<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch\BranchModel;

class BranchController extends Controller
{
    public function index(Request $request) {
        $per_page = ($request->per_page) ? $request->per_page : 10;
        $branch = BranchModel::paginate($per_page);

        return response()->json([
            'success' => true,
            'message' => 'Branch data has been retrivied',
            'result' => $branch
        ], 200);
    }

    public function memberBranch() {
        $branch = BranchModel::whereNotIn('branch_id', [0,6,28,29])->get();

        return response()->json([
            'success' => true,
            'message' => 'Branch member data has been retrivied',
            'result' => $branch
        ], 200);
    }

    public function memberBranchSelect2() {
        
    }
}