<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Member\MemberModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Rules\memberPhoneRule;
use App\Mail\VerifyAccount;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;

class MemberController extends Controller
{
    public function index() {
        //
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'member_name' => 'required',
            'member_photo' => 'required|image',
            'ktp_photo' => 'required|image',
            'member_idcard' => 'numeric',
            'member_dob' => 'required',
            'member_gender' => 'required|in:male,female',
            'member_phone' => 'required|numeric',
            'member_add' => 'required',
            'member_email' => 'required|email|unique:tab_member,member_email',
            'branch_id' => 'required|numeric|exists:tab_branch,branch_id',
            'password' => 'required|min:8|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/',
        ]);

        $validator2 = Validator::make($request->all(), [
            'member_phone' => [new memberPhoneRule]
        ]);

        if($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'error' => $validator->errors()
            ], 400);
        }

        if($validator2->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation error',
                'error' => $validator2->errors()
            ], 400);
        }

        try {
            DB::beginTransaction();

            $member_photo = $request->file('member_photo');
            $member_photo_name = str_replace(' ', '', $request->member_name) . '-' . time() . '.'. $member_photo->getClientOriginalExtension();

            $ktp_photo = $request->file('ktp_photo');
            $ktp_photo_name = str_replace(' ', '', $request->member_name) . '-' . time() . '.' . $ktp_photo->getClientOriginalExtension();

            $data = new Membermodel();

            $data->member_name = $request->member_name;
            $data->member_photo = $member_photo_name;
            $data->member_idcard = $request->member_idcard;
            $data->ktp_photo = $ktp_photo_name;
            $data->member_dob = date('Y-m-d', strtotime($request->member_dob));
            $data->member_gender = $request->member_gender;
            $data->member_phone = $request->member_phone;
            $data->member_add = $request->member_add;
            $data->member_email = $request->member_email;
            $data->branch_id = $request->branch_id;
            $data->password = Hash::make($request->password);
            $data->join_date = date('Y-m-d');
            $data->emp_id = 9999;
            $data->user_id = 9999;
            $data->origin = 'mobile';
            $digit_otp = 4;
            $code_otp = rand(pow(10, $digit_otp-1), pow(10, $digit_otp)-1);
            $data->code_otp = $code_otp;
            $data->due_time_otp = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +5 minutes"));
            
            if($data->save()) {
                DB::commit();

                $member_photo->move(base_path() . '/' . env('MEMBER_PATH_MEDIA'), $member_photo_name);
                $ktp_photo->move(base_path() . '/' . env('MEMBER_KTP_PATH_MEDIA'), $ktp_photo_name);

                // $data->token = Crypt::encrypt($data->member_id);
                // Mail::to($request->member_email)->send(new verifyAccount($data));

                // $message = 'Kode verifikasi akun DSF Anda adalah : ' . $code_otp;
                // Http::get(env('OTP_URL'), [
                //     'username' => env('OTP_USERNAME'),
                //     'password' => env('OTP_PASSWORD'),
                //     'sender' => 'Duo Shine Fitness',
                //     'msisdn' => $request->member_phone,
                //     'message' => $message,
                // ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Signup successfully, please check the OTP code to verify your number phone',
                    'result' => $data
                ], 200);
            }

        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();

            return response()->json([
                'success' => false,
                'message' => 'Query exception',
                'error' => $e
            ], 500);
        } catch (\Exception $e) {
            return $e;
            return response()->json([
                'success' => false,
                'message' => 'Internal server error',
                'error' => $e
            ], 500);
        }
    }
}