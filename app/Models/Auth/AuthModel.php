<?php

namespace App\Models\Auth;
use Illuminate\Database\Eloquent\Model;

class AuthModel extends Model
{
    protected $table = 'tab_user';
    protected $primaryKey = 'user_id';
    const CREATED_AT = 'ts';
    public $timestamps = false;

    protected $fillable = [
        'user_id', 'user_name', 'user_fname', 'role_id', 'branch_id', 'isDeleted', 'ts'
    ];

    protected $hidden = [
        'user_pass', 
    ];
}