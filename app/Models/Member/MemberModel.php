<?php

namespace App\Models\Member;
use Illuminate\Database\Eloquent\Model;

class MemberModel extends Model
{
    protected $table = 'tab_member';
    protected $primaryKey = 'member_id';
    const CREATED_AT = 'ts';
    public $timestamps = false;

    protected $fillable = [
        'member_id', 'member_barcode', 'member_idcard', 'member_name', 'member_photo', 'ktp_photo', 'member_dob', 'member_gender', 'member_phone', 'member_add', 'member_email', 'branch_id', 'join_date', 'isDeleted', 'emp_id', 'user_id', 'src_branch', 'code_otp', 'verified_otp', 'verified_email', 'auth_code', 'reset_token', 'origin', 'ts', 'isActive', 'isAssign', 'password'
    ];

    // protected $hidden = [
    //     'password'
    // ];
}