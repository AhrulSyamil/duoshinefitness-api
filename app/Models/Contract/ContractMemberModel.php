<?php

namespace App\Models\Contract;
use Illuminate\Database\Eloquent\Model;

class ContractMemberModel extends Model
{
    protected $table = 'tr_kontrak';
    protected $primaryKey = 'kontrak_id';
    const CREATED_AT = 'ts';
    const UPDATED_AT = 'updatetime';

    protected $fillable = [
        'member_id', 'member_barcode', 'member_idcard', 'member_name', 'member_photo', 'ktp_photo', 'member_dob', 'member_gender', 'member_phone', 'member_add', 'member_email', 'branch_id', 'join_date', 'isDeleted', 'emp_id', 'user_id', 'src_branch', 'code_otp', 'verified_otp', 'auth_code', 'fcm_token', 'organic', 'ts', 'isActive', 'isAssign'
    ];

    public function paket()
    {
        return $this->belongsTo('App\Models\Paket\PaketModel', 'paket_id', 'paket_id');
    }
}