<?php

namespace App\Models\Branch;
use Illuminate\Database\Eloquent\Model;

class BranchModel extends Model
{
    protected $table = 'tab_branch';
    protected $primaryKey = 'branch_id';
    const CREATED_AT = 'ts';
    public $timestamps = false;

    protected $fillable = [
        'branch_id', 'branch_name', 'id_owner', 'isPrioritas', 'target', 'branch_add', 'branch_city', 'branch_postcode', 'branch_phone', 'isDeleted', 'ts'
    ];
}