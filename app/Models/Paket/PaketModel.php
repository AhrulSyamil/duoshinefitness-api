<?php

namespace App\Models\Paket;
use Illuminate\Database\Eloquent\Model;

class PaketModel extends Model
{
    protected $table = 'tab_paket';
    protected $primaryKey = 'paket_id';
    const CREATED_AT = 'ts';
    public $timestamps = false;

    protected $fillable = [
        'paket_id', 'paket_name', 'paket_value', 'paket_type', 'duration', 'bonus_daftar', 'branch_id', 'paket_status', 'paket_prioritas', 'isDeleted', 'ts'
    ];

    public function contracts()
    {
        return $this->hasMany('App\Models\Contract\ContractMemberModel', 'paket_id', 'paket_id');
    }
}