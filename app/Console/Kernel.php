<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Models\Member\MemberModel;
use Illuminate\Support\Facades\DB;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Carbon\Carbon;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ControllerMakeCommand::class,
        Commands\ModelMakeCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            Log::info('Run job expired contract reminder');

            $member_fcm_token = DB::table('tr_kontrak')
                                ->join('tab_member', 'tab_member.member_id', 'tr_kontrak.member_id')
                                ->whereDate('tr_kontrak.due_date', '=', date('Y-m-d', strtotime('+5 days')))
                                ->whereNotNull('tab_member.fcm_token')
                                ->pluck('tab_member.fcm_token')
                                ->toArray();

            if(count($member_fcm_token) > 0) {
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60*20);

                $notificationBuilder = new PayloadNotificationBuilder('Reminder');
                $notificationBuilder->setBody('The contract will expire in 5 days')
                                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData(['a_data' => 'my_data']);

                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();

                $downstreamResponse = FCM::sendTo($member_fcm_token, $option, $notification, $data);
            }

        })->dailyAt('10:00');
    }
}
