<?php 

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Member\MemberModel;

class memberPhoneRule implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
          return MemberModel::where('member_phone', $value)->where('origin', 'mobile')->get()->count() == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute has already been taken.';
    }
}