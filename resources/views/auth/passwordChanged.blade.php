<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Password Changed | DUO SHINE FITNESS</title>
     <style>
          * {
               padding: 0;
               margin: 0;
               font-family: 'arial';
          }

          .container {
               width: 70%;
               margin: 0 auto;
          }

          .header {
               padding: 15px 0;
               background-color: #ffff00;
               text-align: center;
               border-bottom: 2px solid black;
          }

          .logo {
               font-weight: bold;
               font-size: 35px;
               font-style: italic;
          }

          .content {
               padding: 20px 200px;
               color: #3d4852;
               text-align: center;
          }
     </style>
</head>

<body>
     <div class="container">
          <div class="header">
               <span class="logo">DUO SHINE FITNESS</span>
          </div>
          <div class="content">
               <h3>Hello, {{ ucfirst($user->member_name) }}!</h3>
               <br>
               <p>The password for your Duo Shine Fitness account has successfully been changed...
               </p>
               <br>
               <p>If you did not initiate this change, please contact your administrator immediately.</p>
          </div>
     </div>
</body>

</html>