<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Verify Account | DUO SHINE FITNESS</title>
     <style>
          * {
               padding: 0;
               margin: 0;
               font-family: 'arial';
          }

          .container {
               width: 70%;
               margin: 0 auto;
          }

          .header {
               padding: 15px 0;
               background-color: #ffff00;
               text-align: center;
               border-bottom: 2px solid black;
          }

          .logo {
               font-weight: bold;
               font-size: 35px;
               font-style: italic;
          }

          .content {
               padding: 20px 200px;
               color: #3d4852;
               text-align: center;
          }

          .link {
               display: inline-block;
               margin: 50px 0;
          }

          .footer {
               padding: 20px 200px;
               color: #3d4852;
          }
     </style>
</head>

<body>
     <div class="container">
          <div class="header">
               <span class="logo">DUO SHINE FITNESS</span>
          </div>
          <div class="content">
               <h3>Hello, {{ ucfirst($user->member_name) }}!</h3>
               <br>
               <p>Thanks for your interest in joining Duo Shine Fitness! To complete your registration, we need you to
                    verify your
                    email address.</p>
               <br>
               <a href="{{ url('auth/verify-account') . '/' . $user->token }}" class="link">Verify Email</a>
               <hr>
          </div>
          <div class="footer">
               <small>
                    If you’re having trouble clicking the "Verify Email" link, copy and paste the URL below into your
                    web browser:
               </small>
               <small>
                    <a
                         href="{{ url('auth/verify-acount') . '/' .  $user->token }}">{{ url('auth/verify-account') . '/' . $user->token }}</a>
               </small>
          </div>
     </div>
</body>

</html>