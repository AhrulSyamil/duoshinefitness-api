<!DOCTYPE html>
<html>

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>Reset Password | DUO SHINE FITNESS</title>
     <!-- Tell the browser to be responsive to screen width -->
     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <!-- Bootstrap 3.3.5 -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
     <!-- Font Awesome -->
     <!--link rel="stylesheet" href="http://duoshinefitness.net/media/fontawesome4.5.0/css/font-awesome.min.css"-->
     <!-- Ionicons -->
     <!--link rel="stylesheet" href="http://duoshinefitness.net/media/ionicons2.0.1/css/ionicons.min.css"-->
     <!-- Theme style -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.2/css/AdminLTE.min.css">
     <link rel="stylesheet" href="http://duoshinefitness.net/media/site/css/site.css">

     <noscript>
          <style>
               .wrapper {
                    display: none;
               }

               .warning {
                    height: 100vh;
                    background: black;
                    color: white;
                    margin: 0;
                    padding: 10px 50px 20px 50px;
               }
          </style>
          <div class="warning">
               <h1>WARNING, JavaScript is disabled in your browser !</h1>
               <p>We will not be able to serve you properly with Javascript disabled in your browser.</p>
               <p>Please enabled JavaScript support in your browser and refresh this page before you continue.</p>
               <p>Here are the instructions <a href="http://www.enable-javascript.com/" target="_blank">how to enable
                         JavaScript in your web browser</a>.</p>
               <p>If you are not sure how to do this, please contact your system administrator.</p>
               <p>- DUO SHINE FITNESS -</p>
          </div>
     </noscript>

     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
  <script src="http://duoshinefitness.net/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="http://duoshinefitness.net/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition login-page">

     <section class="login">
          <div class="login-box" style="margin-bottom:30px;">
               <div class="login-logo vertical flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="logo-flip1 flipper">
                         <div class="logo-front front">
                              <img src="http://duoshinefitness.net/media/site/img/logo6.png" style="width:100%;">
                         </div>
                         <div class="logo-back back">
                              <img src="http://duoshinefitness.net/media/site/img/logo7.png" style="width:100%;">
                         </div>
                    </div>
               </div>
               <!-- /.login-logo -->
               <div class="login-box-body">
                    @if (!isset($_GET['success']) && $verified == true)
                    <b>
                         <p class="login-box-msg" style="margin-bottom:10px;">Reset your password</p>
                    </b>
                    <div id="messages">

                    </div>
                    <form id="form" method="get" action="{{ url('auth/verify-reset-password') }}">
                         <input type="hidden" name="token" value="{{ $token }}">
                         <div class="form-group">
                              <input type="password" name="password" id="password" class="form-control"
                                   placeholder="New password" required minlength="8"
                                   pattern="(?=^.{8,30}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$"
                                   title="Your password must be at least 8 characters, consists of uppercase letters, lowercase letters, numbers and symbols">
                         </div>
                         <div class="form-group">
                              <input type="password" name="password_confirmation" id="confirm-password"
                                   class="form-control" placeholder="Confirm new password" required minlength="8"
                                   pattern="(?=^.{8,30}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$"
                                   title="Your password must be at least 8 characters, consists of uppercase letters, lowercase letters, numbers and symbols">
                         </div>
                         <div class="row" style="margin-top:10px">
                              <div class="col-xs-12">
                                   <button type="submit" id="submit" class="btn btn-primary btn-block btn-flat btn-go"
                                        style="background-color:#ffff00!important">Change your password</button>
                              </div>
                         </div>
                    </form>
                    @else
                    @if (isset($_GET['success']) == 'true')
                    <div class="alert alert-success">{{ $_GET['message'] }}</div>
                    @elseif(isset($_GET['success']) == 'false')
                    <div class="alert alert-danger">{{ $_GET['message'] }}</div>
                    @else
                    <div class="alert alert-danger">Not verified, please do a forgot password again</div>
                    @endif
                    @endif
               </div>
               <!-- /.login-box-body -->
          </div>
          <!-- /.login-box -->
     </section>
     <section class="footer">
          <div>Copyright &copy; 2016-2020. <span>DUO SHINE FITNESS</span>. All rights reserved.</div>
     </section>

     <!-- jQuery 2.1.4 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
     <!-- Bootstrap 3.3.5 -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
     <!--script src="https://www.google.com/recaptcha/api.js"></script-->
     <script>
          // @if(isset($_GET['success']))
          //      var message = '{{ $_GET["message"] }}';
          //      @if($_GET['success'] == 'true')
          //           $('#messages').html(`<div class="alert alert-success">`+ message +`</div>`);
          //           setTimeout(() => {
          //                window.location = 'https://duoshinefitness.net';
          //           }, 2000);
          //      @else
          //           $('#messages').html(`<div class="alert alert-danger">`+ message +`</div>`);
          //      @endif
          // @endif
          $(document).on('keyup', '#password, #confirm-password', function() {
               const password = document.querySelector('#password');
               const confirm = document.querySelector('#confirm-password');
               if (confirm.value === password.value) {
                    confirm.setCustomValidity('');
               } else {
                    confirm.setCustomValidity('Passwords do not match');
               }
          });
     </script>
</body>

</html>